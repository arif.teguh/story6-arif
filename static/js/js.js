$(document).ready(function(){

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  //coba cari tahu, mengapa kita perlu mengambil csrftoken dari cookie
  var csrftoken = getCookie('csrftoken');
  //token yang ada di variable csrftoken kalian masukan dipassing ke dalam fungsi xhr.setRequestHeader("X-CSRFToken", csrftoken);
  //coba cari tahu, kenapa kita perlu mensetup csrf token terlebih dahulu sebelum melakukan request post ke views django

  //yang disarankan dari dokumentasi django nya
  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });

  $("#tombolsubmit").fadeTo(0, 0.2);
  $("#tombolsubmit").prop("disabled", true);

  //setup before functions
  var timerketik;                //timer identifier
  var waktuketik = 2000;  //time in ms, 5 second for example
  var $input = $(":input");

  //on keyup, start the countdown
  $input.on('keyup', function () {
    clearTimeout(timerketik);
    timerketik = setTimeout(selesaiketik, waktuketik);
  });

  //on keydown, clear the countdown
  $input.on('keydown', function () {
    clearTimeout(timerketik);
  });

  //user is "finished typing," do something
  function selesaiketik () {
    $.ajax({
      method : "POST",
      url : "/cek_email/",
      data : {email:$("#id_email").val()},
      dataType : "json",
      success : function (datajson) {
        var terdaftar = datajson.email_is_taken;
        console.log(terdaftar);
        var nama_cek = $("#id_nama").val().length > 0;
        var email_cek = $("#id_email").val().length > 0;
        var pass_cek = $("#id_password").val().length > 0;
        var valid = nama_cek && email_cek && pass_cek;
        if (terdaftar) {
          alert('Email sudah terdaftar.')
        } else if (email_cek.length != 0) {
          $("#tombolsubmit").fadeTo(600, 1);
          $("#tombolsubmit").prop("disabled", false);
        }
      },

      error: function (error) {
        console.log("Data JSON tidak berhasil diakses");
      },
    });
  };

  $("#tombolsubmit").click(function(){
    $.ajax({
      method : "POST",
      data : $('form').serialize(),
      success : function (datajson) {
        alert("Data berhasil tersimpan!")
      },
      error: function (error) {
      },
    });
  });

  //Ganti tema


 
});
