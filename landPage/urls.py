from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('jeson/', views.jeson),
    path('login/', views.berhasilLogin),
    path('logout/', views.logout),
]